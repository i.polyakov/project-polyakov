<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if ($APPLICATION->GetCurPage(true) == SITE_DIR."index.php"):?>
<div class="mb-5">
	<?$APPLICATION->IncludeComponent("bitrix:search.title", "bootstrap_v4", array(
			"NUM_CATEGORIES" => "1",
			"TOP_COUNT" => "5",
			"CHECK_DATES" => "N",
			"SHOW_OTHERS" => "N",
			"PAGE" => SITE_DIR."catalog/",
			"CATEGORY_0_TITLE" => "Товары" ,
			"CATEGORY_0" => array(
				0 => "iblock_catalog",
			),
			"CATEGORY_0_iblock_catalog" => array(
				0 => "all",
			),
			"CATEGORY_OTHERS_TITLE" => "Прочее",
			"SHOW_INPUT" => "Y",
			"INPUT_ID" => "title-search-input",
			"CONTAINER_ID" => "search",
			"PRICE_CODE" => array(
				0 => "BASE",
			),
			"SHOW_PREVIEW" => "Y",
			"PREVIEW_WIDTH" => "75",
			"PREVIEW_HEIGHT" => "75",
			"CONVERT_CURRENCY" => "Y"
		),
		false
	);?>
</div>
<?endif?>

<div class="mb-5">
	<h3>Мы в соцсетях</h3>
	<?$APPLICATION->IncludeComponent(
		"bitrix:main.include",
		"",
		Array(
			"AREA_FILE_SHOW" => "file",
			"PATH" => SITE_DIR."include/socnet_sidebar.php",
			"AREA_FILE_RECURSIVE" => "N",
			"EDIT_MODE" => "html",
		),
		false,
		Array('HIDE_ICONS' => 'Y')
	);?>
</div>

<div class="mb-5 d-block d-sm-none">
	<?$APPLICATION->IncludeComponent(
		"bitrix:main.include",
		"",
		Array(
			"AREA_FILE_SHOW" => "file",
			"PATH" => SITE_DIR."include/sender.php",
			"AREA_FILE_RECURSIVE" => "N",
			"EDIT_MODE" => "html",
		),
		false,
		Array('HIDE_ICONS' => 'Y')
	);?>
</div>

<div class="mb-5">
	<?$APPLICATION->IncludeComponent(
		"bitrix:main.include",
		"",
		Array(
			"AREA_FILE_SHOW" => "file",
			"PATH" => SITE_DIR."include/about.php",
			"AREA_FILE_RECURSIVE" => "N",
			"EDIT_MODE" => "html",
		),
		false,
		Array('HIDE_ICONS' => 'N')
	);?>
</div>

<div class="mb-5">
	<?$APPLICATION->IncludeComponent(
		"bitrix:main.include",
		"",
		Array(
			"AREA_FILE_SHOW" => "file",
			"PATH" => SITE_DIR."include/twitter.php",
			"AREA_FILE_RECURSIVE" => "N",
			"EDIT_MODE" => "html",
		),
		false,
		Array('HIDE_ICONS' => 'N')
	);?>
</div>

<div class="mb-5">
	<?$APPLICATION->IncludeComponent(
		"bitrix:main.include",
		"",
		Array(
			"AREA_FILE_SHOW" => "file",
			"PATH" => SITE_DIR."include/info.php",
			"AREA_FILE_RECURSIVE" => "N",
			"EDIT_MODE" => "html",
		),
		false,
		Array('HIDE_ICONS' => 'N')
	);?>
</div>/local/templates/my/components/bitrix/sale.basket.basket/bootstrap_v5/template.php:229  - 02-02-2021 20:02:20 --------------------> 
123

/ajax/errors.php:8  - 02-02-2021 20:02:14 --------------------> 
40

/ajax/errors.php:8  - 02-02-2021 20:02:17 --------------------> 
40

/ajax/addBasket.php:7  - 02-02-2021 20:02:09 --------------------> 
40

/ajax/addBasket.php:7  - 11-02-2021 19:02:38 --------------------> 
40

/ajax/ajax_primelabs_cdek.php:17  - 11-02-2021 20:02:39 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 11-02-2021 20:02:29 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 11-02-2021 20:02:21 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:27  - 11-02-2021 20:02:21 --------------------> 
1234

/ajax/ajax_primelabs_cdek.php:27  - 11-02-2021 20:02:21 --------------------> 
1234

/ajax/ajax_primelabs_cdek.php:27  - 11-02-2021 20:02:21 --------------------> 
1234

/ajax/ajax_primelabs_cdek.php:27  - 11-02-2021 20:02:21 --------------------> 
1234

/ajax/ajax_primelabs_cdek.php:27  - 11-02-2021 20:02:21 --------------------> 
1234

/ajax/ajax_primelabs_cdek.php:27  - 11-02-2021 20:02:21 --------------------> 
1234

/ajax/ajax_primelabs_cdek.php:27  - 11-02-2021 20:02:21 --------------------> 
1234

/ajax/ajax_primelabs_cdek.php:27  - 11-02-2021 20:02:21 --------------------> 
1234

/ajax/ajax_primelabs_cdek.php:18  - 11-02-2021 20:02:09 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:27  - 11-02-2021 20:02:09 --------------------> 
Array
(
    [ID] => 34
    [~ID] => 34
    [NAME] => Туфли Полет Валькирии
    [~NAME] => Туфли Полет Валькирии
    [XML_ID] => 511
    [~XML_ID] => 511
)


/ajax/ajax_primelabs_cdek.php:27  - 11-02-2021 20:02:09 --------------------> 
Array
(
    [ID] => 9
    [~ID] => 9
    [NAME] => Платье Ночная Жизнь
    [~NAME] => Платье Ночная Жизнь
    [XML_ID] => 271
    [~XML_ID] => 271
)


/ajax/ajax_primelabs_cdek.php:27  - 11-02-2021 20:02:09 --------------------> 
Array
(
    [ID] => 4
    [~ID] => 4
    [NAME] => Штаны Полосатый Рейс
    [~NAME] => Штаны Полосатый Рейс
    [XML_ID] => 177
    [~XML_ID] => 177
)


/ajax/ajax_primelabs_cdek.php:27  - 11-02-2021 20:02:09 --------------------> 
Array
(
    [ID] => 16
    [~ID] => 16
    [NAME] => Нижнее белье Розовое Смущенье
    [~NAME] => Нижнее белье Розовое Смущенье
    [XML_ID] => 341
    [~XML_ID] => 341
)


/ajax/ajax_primelabs_cdek.php:27  - 11-02-2021 20:02:09 --------------------> 
Array
(
    [ID] => 24
    [~ID] => 24
    [NAME] => Спортивный Костюм Огонь в Ночи
    [~NAME] => Спортивный Костюм Огонь в Ночи
    [XML_ID] => 413
    [~XML_ID] => 413
)


/ajax/ajax_primelabs_cdek.php:27  - 11-02-2021 20:02:09 --------------------> 
Array
(
    [ID] => 38
    [~ID] => 38
    [NAME] => Ремень Элегантность
    [~NAME] => Ремень Элегантность
    [XML_ID] => 610
    [~XML_ID] => 610
)


/ajax/ajax_primelabs_cdek.php:27  - 11-02-2021 20:02:09 --------------------> 
Array
(
    [ID] => 37
    [~ID] => 37
    [NAME] => Ремень Класика
    [~NAME] => Ремень Класика
    [XML_ID] => 601
    [~XML_ID] => 601
)


/ajax/ajax_primelabs_cdek.php:27  - 11-02-2021 20:02:09 --------------------> 
Array
(
    [ID] => 39
    [~ID] => 39
    [NAME] => Ремень Строчка
    [~NAME] => Ремень Строчка
    [XML_ID] => 619
    [~XML_ID] => 619
)


/ajax/ajax_primelabs_cdek.php:18  - 11-02-2021 20:02:01 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 11-02-2021 20:02:39 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 11-02-2021 20:02:11 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 11-02-2021 20:02:35 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 11-02-2021 20:02:21 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 11-02-2021 20:02:02 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 11-02-2021 20:02:06 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 11-02-2021 20:02:14 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 11-02-2021 20:02:38 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 11-02-2021 20:02:45 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 11-02-2021 20:02:09 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 11-02-2021 20:02:20 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 11-02-2021 20:02:01 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 11-02-2021 20:02:03 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 11-02-2021 20:02:22 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 11-02-2021 20:02:27 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 09:02:18 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 09:02:19 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 09:02:21 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 09:02:25 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 09:02:40 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 09:02:43 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 11:02:14 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 11:02:30 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 11:02:35 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 11:02:37 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 11:02:42 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 11:02:51 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 11:02:54 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 11:02:58 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 11:02:08 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 11:02:15 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 11:02:17 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 11:02:01 --------------------> 
123

/ajax/addBasket.php:7  - 23-02-2021 11:02:03 --------------------> 
40

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 11:02:20 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 11:02:22 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 11:02:47 --------------------> 
123

/ajax/addBasket.php:7  - 23-02-2021 11:02:53 --------------------> 


/ajax/addBasket.php:7  - 23-02-2021 11:02:41 --------------------> 


/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 11:02:43 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 11:02:46 --------------------> 
123

/ajax/addBasket.php:7  - 23-02-2021 11:02:49 --------------------> 


/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 12:02:53 --------------------> 
123

/ajax/addBasket.php:7  - 23-02-2021 12:02:57 --------------------> 
[33] Пантолеты Кости на Пляже 502

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 12:02:46 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 12:02:48 --------------------> 
123

/ajax/addBasket.php:7  - 23-02-2021 12:02:51 --------------------> 
[33] Пантолеты Кости на Пляже 502

/ajax/addBasket.php:7  - 23-02-2021 12:02:55 --------------------> 
[33] Пантолеты Кости на Пляже 502

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 12:02:35 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 12:02:42 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 12:02:42 --------------------> 
123

/ajax/addBasket.php:7  - 23-02-2021 12:02:55 --------------------> 
[29] Домашние Тапочки Розовый Рай 465

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 12:02:11 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 12:02:11 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 12:02:11 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 12:02:15 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 12:02:15 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 12:02:18 --------------------> 
123

/ajax/addBasket.php:7  - 23-02-2021 12:02:25 --------------------> 
[33] Пантолеты Кости на Пляже 502

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 12:02:05 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 12:02:05 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 12:02:05 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 12:02:08 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 12:02:08 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 12:02:11 --------------------> 
123

/ajax/addBasket.php:7  - 23-02-2021 12:02:14 --------------------> 
Array
(
    [0] => NaN
    [1] => NaN
    [2] => NaN
)


/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 12:02:58 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 12:02:58 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 12:02:59 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 12:02:59 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 12:02:59 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 12:02:31 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 12:02:34 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 12:02:34 --------------------> 
123

/ajax/addBasket.php:7  - 23-02-2021 12:02:39 --------------------> 
Array
(
    [0] => [29] Домашние Тапочки Розовый Рай 465
    [1] => [33] Пантолеты Кости на Пляже 502
    [2] => [32] Туфли Вечерняя Бабочка 493
)


/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 12:02:04 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 12:02:04 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 12:02:05 --------------------> 
123

/ajax/addBasket.php:13  - 23-02-2021 12:02:09 --------------------> 
Array
(
    [0] => Array
        (
        )

    [1] => Array
        (
        )

)


/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 12:02:18 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 12:02:18 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 12:02:18 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 12:02:18 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 12:02:19 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 12:02:19 --------------------> 
123

/ajax/addBasket.php:13  - 23-02-2021 12:02:23 --------------------> 
Array
(
    [0] => Array
        (
        )

    [1] => Array
        (
        )

    [2] => Array
        (
        )

)


/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 12:02:48 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 12:02:48 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 12:02:49 --------------------> 
123

/ajax/addBasket.php:13  - 23-02-2021 12:02:51 --------------------> 
Array
(
    [0] => Array
        (
        )

    [1] => Array
        (
        )

)


/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 12:02:21 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 12:02:22 --------------------> 
123

/ajax/addBasket.php:10  - 23-02-2021 12:02:24 --------------------> 
[33] Пантолеты Кости на Пляже 502

/ajax/addBasket.php:10  - 23-02-2021 12:02:24 --------------------> 
[29] Домашние Тапочки Розовый Рай 465

/ajax/addBasket.php:14  - 23-02-2021 12:02:24 --------------------> 
Array
(
    [0] => Array
        (
        )

    [1] => Array
        (
        )

)


/ajax/addBasket.php:10  - 23-02-2021 12:02:31 --------------------> 


/ajax/addBasket.php:14  - 23-02-2021 12:02:31 --------------------> 
Array
(
    [0] => Array
        (
        )

)


/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 12:02:31 --------------------> 
123

/ajax/addBasket.php:10  - 23-02-2021 12:02:35 --------------------> 
[29] Домашние Тапочки Розовый Рай 465

/ajax/addBasket.php:10  - 23-02-2021 12:02:35 --------------------> 
[33] Пантолеты Кости на Пляже 502

/ajax/addBasket.php:14  - 23-02-2021 12:02:35 --------------------> 
Array
(
    [0] => Array
        (
        )

    [1] => Array
        (
        )

)


/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 12:02:12 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 12:02:13 --------------------> 
123

/ajax/addBasket.php:10  - 23-02-2021 12:02:15 --------------------> 
[33] Пантолеты Кости на Пляже 502

/ajax/addBasket.php:10  - 23-02-2021 12:02:15 --------------------> 
[29] Домашние Тапочки Розовый Рай 465

/ajax/addBasket.php:14  - 23-02-2021 12:02:15 --------------------> 
Array
(
    [0] => Array
        (
        )

    [1] => Array
        (
        )

)


/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 12:02:18 --------------------> 
123

/ajax/addBasket.php:10  - 23-02-2021 12:02:20 --------------------> 
[33] Пантолеты Кости на Пляже 502

/ajax/addBasket.php:14  - 23-02-2021 12:02:20 --------------------> 
Array
(
    [0] => Array
        (
        )

)


/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 12:02:06 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 12:02:06 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 12:02:07 --------------------> 
123

/ajax/addBasket.php:10  - 23-02-2021 12:02:09 --------------------> 
[33] Пантолеты Кости на Пляже 502

/ajax/addBasket.php:10  - 23-02-2021 12:02:09 --------------------> 
[29] Домашние Тапочки Розовый Рай 465

/ajax/addBasket.php:14  - 23-02-2021 12:02:09 --------------------> 
Array
(
    [0] => Array
        (
            [0] => [33]
            [1] => 33
        )

    [1] => Array
        (
            [0] => [29]
            [1] => 29
        )

)


/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 13:02:33 --------------------> 
123

/ajax/addBasket.php:20  - 23-02-2021 13:02:35 --------------------> 
[33] Пантолеты Кости на Пляже 502

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 13:02:00 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 13:02:01 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 13:02:01 --------------------> 
123

/ajax/addBasket.php:20  - 23-02-2021 13:02:03 --------------------> 
[33] Пантолеты Кости на Пляже 502

/ajax/addBasket.php:23  - 23-02-2021 13:02:03 --------------------> 
Array
(
    [0] => [33]
    [1] => 33
)


/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 13:02:02 --------------------> 
123

/ajax/addBasket.php:20  - 23-02-2021 13:02:06 --------------------> 
[29] Домашние Тапочки Розовый Рай 465

/ajax/addBasket.php:23  - 23-02-2021 13:02:06 --------------------> 
Array
(
    [0] => [29]
    [1] => 29
)


/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 13:02:40 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 13:02:41 --------------------> 
123

/ajax/addBasket.php:20  - 23-02-2021 13:02:43 --------------------> 
[29] Домашние Тапочки Розовый Рай 465

/ajax/addBasket.php:23  - 23-02-2021 13:02:43 --------------------> 
Array
(
    [0] => [29]
    [1] => 29
)


/ajax/addBasket.php:27  - 23-02-2021 13:02:43 --------------------> 
123

/ajax/addBasket.php:31  - 23-02-2021 13:02:43 --------------------> 
1234

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 13:02:37 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 13:02:37 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 13:02:37 --------------------> 
123

/ajax/addBasket.php:20  - 23-02-2021 13:02:41 --------------------> 
[33] Пантолеты Кости на Пляже 502

/ajax/addBasket.php:23  - 23-02-2021 13:02:41 --------------------> 
Array
(
    [0] => [33]
    [1] => 33
)


/ajax/addBasket.php:27  - 23-02-2021 13:02:41 --------------------> 
123

/ajax/addBasket.php:31  - 23-02-2021 13:02:41 --------------------> 
1234

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 13:02:03 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 13:02:04 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 13:02:04 --------------------> 
123

/ajax/addBasket.php:20  - 23-02-2021 13:02:07 --------------------> 
[33] Пантолеты Кости на Пляже 502

/ajax/addBasket.php:23  - 23-02-2021 13:02:07 --------------------> 
Array
(
    [0] => [33]
    [1] => 33
)


/ajax/addBasket.php:27  - 23-02-2021 13:02:07 --------------------> 
123

/ajax/addBasket.php:31  - 23-02-2021 13:02:07 --------------------> 
1234

/ajax/addBasket.php:33  - 23-02-2021 13:02:07 --------------------> 
12345

/ajax/addBasket.php:20  - 23-02-2021 13:02:07 --------------------> 
[29] Домашние Тапочки Розовый Рай 465

/ajax/addBasket.php:23  - 23-02-2021 13:02:07 --------------------> 
Array
(
    [0] => [29]
    [1] => 29
)


/ajax/addBasket.php:27  - 23-02-2021 13:02:07 --------------------> 
123

/ajax/addBasket.php:31  - 23-02-2021 13:02:07 --------------------> 
1234

/ajax/addBasket.php:33  - 23-02-2021 13:02:07 --------------------> 
12345

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 13:02:51 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 13:02:52 --------------------> 
123

/ajax/addBasket.php:20  - 23-02-2021 13:02:56 --------------------> 
[33] Пантолеты Кости на Пляже 502

/ajax/addBasket.php:23  - 23-02-2021 13:02:56 --------------------> 
Array
(
    [0] => [33]
    [1] => 33
)


/ajax/addBasket.php:27  - 23-02-2021 13:02:56 --------------------> 
123

/ajax/addBasket.php:31  - 23-02-2021 13:02:56 --------------------> 
1234

/ajax/addBasket.php:35  - 23-02-2021 13:02:56 --------------------> 
12345

/ajax/addBasket.php:20  - 23-02-2021 13:02:56 --------------------> 
[29] Домашние Тапочки Розовый Рай 465

/ajax/addBasket.php:23  - 23-02-2021 13:02:56 --------------------> 
Array
(
    [0] => [29]
    [1] => 29
)


/ajax/addBasket.php:27  - 23-02-2021 13:02:56 --------------------> 
123

/ajax/addBasket.php:31  - 23-02-2021 13:02:56 --------------------> 
1234

/ajax/addBasket.php:35  - 23-02-2021 13:02:56 --------------------> 
12345

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 13:02:09 --------------------> 
123

/ajax/addBasket.php:20  - 23-02-2021 13:02:11 --------------------> 
[33] Пантолеты Кости на Пляже 502

/ajax/addBasket.php:23  - 23-02-2021 13:02:11 --------------------> 
Bitrix\Sale\Basket Object
(
    [siteId:protected] => s1
    [fUserId:protected] => 1
    [order:protected] => 
    [basketItemIndexMap:protected] => Array
        (
            [1] => 0
        )

    [maxItemSort:protected] => 100
    [isLoadForFUserId:Bitrix\Sale\BasketBase:private] => 1
    [isSaveExecuting:protected] => 
    [index:Bitrix\Sale\Internals\EntityCollection:private] => 0
    [isClone:protected] => 
    [anyItemDeleted:protected] => 
    [collection:protected] => Array
        (
            [0] => Bitrix\Sale\BasketItem Object
                (
                    [bundleCollection:Bitrix\Sale\BasketItem:private] => 
                    [propertyCollection:protected] => 
                    [calculatedFields:protected] => Bitrix\Sale\Internals\Fields Object
                        (
                            [values:protected] => Array
                                (
                                )

                            [changedValues:protected] => Array
                                (
                                )

                            [originalValues:Bitrix\Sale\Internals\Fields:private] => Array
                                (
                                )

                            [customFields:Bitrix\Sale\Internals\Fields:private] => Array
                                (
                                )

                            [isClone:protected] => 
                        )

                    [provider:protected] => 
                    [internalId:protected] => 1
                    [collection:protected] => Bitrix\Sale\Basket Object
 *RECURSION*
                    [internalIndex:protected] => 0
                    [isClone:protected] => 
                    [fields:protected] => Bitrix\Sale\Internals\Fields Object
                        (
                            [values:protected] => Array
                                (
                                    [ID] => 1
                                    [LID] => s1
                                    [MODULE] => catalog
                                    [PRODUCT_ID] => 39
                                    [QUANTITY] => 3.0000
                                    [WEIGHT] => 0.00
                                    [DELAY] => N
                                    [CAN_BUY] => Y
                                    [PRICE] => 1
                                    [CUSTOM_PRICE] => N
                                    [BASE_PRICE] => 1
                                    [PRODUCT_PRICE_ID] => 1
                                    [PRICE_TYPE_ID] => 1
                                    [CURRENCY] => RUB
                                    [BARCODE_MULTI] => N
                                    [RESERVED] => N
                                    [RESERVE_QUANTITY] => 
                                    [NAME] => Ремень Строчка
                                    [CATALOG_XML_ID] => clothes
                                    [VAT_RATE] => 0.0000
                                    [NOTES] => Розничная цена
                                    [DISCOUNT_PRICE] => 0
                                    [PRODUCT_PROVIDER_CLASS] => \Bitrix\Catalog\Product\CatalogProvider
                                    [CALLBACK_FUNC] => 
                                    [ORDER_CALLBACK_FUNC] => 
                                    [PAY_CALLBACK_FUNC] => 
                                    [CANCEL_CALLBACK_FUNC] => 
                                    [DIMENSIONS] => a:3:{s:5:"WIDTH";N;s:6:"HEIGHT";N;s:6:"LENGTH";N;}
                                    [TYPE] => 
                                    [SET_PARENT_ID] => 
                                    [DETAIL_PAGE_URL] => /catalog/men-s-belts/strap-line/
                                    [FUSER_ID] => 1
                                    [MEASURE_CODE] => 796
                                    [MEASURE_NAME] => шт
                                    [ORDER_ID] => 
                                    [DATE_INSERT] => Bitrix\Main\Type\DateTime Object
                                        (
                                            [value:protected] => DateTime Object
                                                (
                                                    [date] => 2021-02-02 17:59:55.000000
                                                    [timezone_type] => 3
                                                    [timezone] => Europe/Berlin
                                                )

                                        )

                                    [DATE_UPDATE] => Bitrix\Main\Type\DateTime Object
                                        (
                                            [value:protected] => DateTime Object
                                                (
                                                    [date] => 2021-02-02 18:17:16.000000
                                                    [timezone_type] => 3
                                                    [timezone] => Europe/Berlin
                                                )

                                        )

                                    [PRODUCT_XML_ID] => 619
                                    [SUBSCRIBE] => N
                                    [RECOMMENDATION] => 
                                    [VAT_INCLUDED] => Y
                                    [SORT] => 100
                                    [DATE_REFRESH] => 
                                    [DISCOUNT_NAME] => 
                                    [DISCOUNT_VALUE] => 
                                    [DISCOUNT_COUPON] => 
                                    [XML_ID] => bx_6019850b21ebb
                                    [MARKING_CODE_GROUP] => 
                                )

                            [changedValues:protected] => Array
                                (
                                )

                            [originalValues:Bitrix\Sale\Internals\Fields:private] => Array
                                (
                                )

                            [customFields:Bitrix\Sale\Internals\Fields:private] => Array
                                (
                                )

                            [isClone:protected] => 
                        )

                    [eventName:protected] => 
                )

        )

)


/ajax/addBasket.php:27  - 23-02-2021 13:02:11 --------------------> 
123

/ajax/addBasket.php:31  - 23-02-2021 13:02:11 --------------------> 
1234

/ajax/addBasket.php:35  - 23-02-2021 13:02:11 --------------------> 
12345

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 13:02:02 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 13:02:03 --------------------> 
123

/ajax/addBasket.php:20  - 23-02-2021 13:02:05 --------------------> 
[33] Пантолеты Кости на Пляже 502

/ajax/addBasket.php:23  - 23-02-2021 13:02:05 --------------------> 
Array
(
    [0] => [33]
    [1] => 33
)


/ajax/addBasket.php:27  - 23-02-2021 13:02:05 --------------------> 
123

/ajax/addBasket.php:31  - 23-02-2021 13:02:05 --------------------> 
1234

/ajax/addBasket.php:35  - 23-02-2021 13:02:05 --------------------> 
12345

/ajax/addBasket.php:20  - 23-02-2021 13:02:05 --------------------> 
[29] Домашние Тапочки Розовый Рай 465

/ajax/addBasket.php:23  - 23-02-2021 13:02:05 --------------------> 
Array
(
    [0] => [29]
    [1] => 29
)


/ajax/addBasket.php:27  - 23-02-2021 13:02:05 --------------------> 
123

/ajax/addBasket.php:31  - 23-02-2021 13:02:05 --------------------> 
1234

/ajax/addBasket.php:35  - 23-02-2021 13:02:05 --------------------> 
12345

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 13:02:21 --------------------> 
123

/ajax/addBasket.php:20  - 23-02-2021 13:02:23 --------------------> 
[33] Пантолеты Кости на Пляже 502

/ajax/addBasket.php:23  - 23-02-2021 13:02:23 --------------------> 
Array
(
    [0] => [33]
    [1] => 33
)


/ajax/addBasket.php:27  - 23-02-2021 13:02:23 --------------------> 
123

/ajax/addBasket.php:31  - 23-02-2021 13:02:23 --------------------> 
1234

/ajax/addBasket.php:34  - 23-02-2021 13:02:23 --------------------> 
Array
(
    [0] => Товар не найден
)


/ajax/addBasket.php:39  - 23-02-2021 13:02:23 --------------------> 
12345

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 13:02:03 --------------------> 
123

/ajax/addBasket.php:20  - 23-02-2021 13:02:05 --------------------> 
[33] Пантолеты Кости на Пляже 502

/ajax/addBasket.php:23  - 23-02-2021 13:02:05 --------------------> 
Array
(
    [0] => [33]
    [1] => 33
)


/ajax/addBasket.php:27  - 23-02-2021 13:02:05 --------------------> 
123

/ajax/addBasket.php:31  - 23-02-2021 13:02:07 --------------------> 
1234

/ajax/addBasket.php:39  - 23-02-2021 13:02:07 --------------------> 
12345

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 13:02:08 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 13:02:08 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 23-02-2021 13:02:12 --------------------> 
123

/ajax/addBasket.php:20  - 23-02-2021 13:02:14 --------------------> 
[33] Пантолеты Кости на Пляже 502

/ajax/addBasket.php:23  - 23-02-2021 13:02:14 --------------------> 
Array
(
    [0] => [33]
    [1] => 33
)


/ajax/addBasket.php:27  - 23-02-2021 13:02:14 --------------------> 
123

/ajax/addBasket.php:31  - 23-02-2021 13:02:15 --------------------> 
1234

/ajax/addBasket.php:39  - 23-02-2021 13:02:15 --------------------> 
12345

/ajax/addBasket.php:20  - 23-02-2021 13:02:15 --------------------> 
[29] Домашние Тапочки Розовый Рай 465

/ajax/addBasket.php:23  - 23-02-2021 13:02:15 --------------------> 
Array
(
    [0] => [29]
    [1] => 29
)


/ajax/addBasket.php:27  - 23-02-2021 13:02:15 --------------------> 
123

/ajax/addBasket.php:31  - 23-02-2021 13:02:15 --------------------> 
1234

/ajax/addBasket.php:39  - 23-02-2021 13:02:15 --------------------> 
12345

/ajax/addBasket.php:20  - 25-02-2021 17:02:43 --------------------> 


/ajax/addBasket.php:23  - 25-02-2021 17:02:43 --------------------> 
Array
(
)


/ajax/addBasket.php:27  - 25-02-2021 17:02:43 --------------------> 
123

/ajax/addBasket.php:31  - 25-02-2021 17:02:43 --------------------> 
1234

/ajax/addBasket.php:34  - 25-02-2021 17:02:43 --------------------> 
Array
(
    [0] => Товар не найден
)


/ajax/addBasket.php:39  - 25-02-2021 17:02:43 --------------------> 
12345

/ajax/addBasket.php:20  - 25-02-2021 17:02:45 --------------------> 


/ajax/addBasket.php:23  - 25-02-2021 17:02:45 --------------------> 
Array
(
)


/ajax/addBasket.php:27  - 25-02-2021 17:02:45 --------------------> 
123

/ajax/addBasket.php:31  - 25-02-2021 17:02:45 --------------------> 
1234

/ajax/addBasket.php:34  - 25-02-2021 17:02:45 --------------------> 
Array
(
    [0] => Товар не найден
)


/ajax/addBasket.php:39  - 25-02-2021 17:02:45 --------------------> 
12345

/ajax/addBasket.php:20  - 25-02-2021 17:02:46 --------------------> 


/ajax/addBasket.php:23  - 25-02-2021 17:02:46 --------------------> 
Array
(
)


/ajax/addBasket.php:27  - 25-02-2021 17:02:46 --------------------> 
123

/ajax/addBasket.php:31  - 25-02-2021 17:02:46 --------------------> 
1234

/ajax/addBasket.php:34  - 25-02-2021 17:02:46 --------------------> 
Array
(
    [0] => Товар не найден
)


/ajax/addBasket.php:39  - 25-02-2021 17:02:46 --------------------> 
12345

/ajax/addBasket.php:20  - 25-02-2021 17:02:47 --------------------> 


/ajax/addBasket.php:23  - 25-02-2021 17:02:47 --------------------> 
Array
(
)


/ajax/addBasket.php:27  - 25-02-2021 17:02:47 --------------------> 
123

/ajax/addBasket.php:31  - 25-02-2021 17:02:47 --------------------> 
1234

/ajax/addBasket.php:34  - 25-02-2021 17:02:47 --------------------> 
Array
(
    [0] => Товар не найден
)


/ajax/addBasket.php:39  - 25-02-2021 17:02:47 --------------------> 
12345

/ajax/addBasket.php:20  - 25-02-2021 17:02:48 --------------------> 


/ajax/addBasket.php:23  - 25-02-2021 17:02:48 --------------------> 
Array
(
)


/ajax/addBasket.php:27  - 25-02-2021 17:02:48 --------------------> 
123

/ajax/addBasket.php:31  - 25-02-2021 17:02:48 --------------------> 
1234

/ajax/addBasket.php:34  - 25-02-2021 17:02:48 --------------------> 
Array
(
    [0] => Товар не найден
)


/ajax/addBasket.php:39  - 25-02-2021 17:02:48 --------------------> 
12345

/ajax/addBasket.php:20  - 25-02-2021 17:02:48 --------------------> 


/ajax/addBasket.php:23  - 25-02-2021 17:02:48 --------------------> 
Array
(
)


/ajax/addBasket.php:27  - 25-02-2021 17:02:48 --------------------> 
123

/ajax/addBasket.php:31  - 25-02-2021 17:02:48 --------------------> 
1234

/ajax/addBasket.php:34  - 25-02-2021 17:02:48 --------------------> 
Array
(
    [0] => Товар не найден
)


/ajax/addBasket.php:39  - 25-02-2021 17:02:48 --------------------> 
12345

/ajax/addBasket.php:20  - 25-02-2021 17:02:48 --------------------> 


/ajax/addBasket.php:23  - 25-02-2021 17:02:48 --------------------> 
Array
(
)


/ajax/addBasket.php:27  - 25-02-2021 17:02:48 --------------------> 
123

/ajax/addBasket.php:31  - 25-02-2021 17:02:48 --------------------> 
1234

/ajax/addBasket.php:34  - 25-02-2021 17:02:48 --------------------> 
Array
(
    [0] => Товар не найден
)


/ajax/addBasket.php:39  - 25-02-2021 17:02:48 --------------------> 
12345

/ajax/addBasket.php:20  - 25-02-2021 17:02:52 --------------------> 


/ajax/addBasket.php:23  - 25-02-2021 17:02:52 --------------------> 
Array
(
)


/ajax/addBasket.php:27  - 25-02-2021 17:02:52 --------------------> 
123

/ajax/addBasket.php:31  - 25-02-2021 17:02:52 --------------------> 
1234

/ajax/addBasket.php:34  - 25-02-2021 17:02:52 --------------------> 
Array
(
    [0] => Товар не найден
)


/ajax/addBasket.php:39  - 25-02-2021 17:02:52 --------------------> 
12345

/ajax/ajax_primelabs_cdek.php:18  - 25-02-2021 18:02:10 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 25-02-2021 18:02:45 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 25-02-2021 18:02:45 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 25-02-2021 18:02:43 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 25-02-2021 18:02:43 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 25-02-2021 18:02:43 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 25-02-2021 18:02:51 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 25-02-2021 18:02:51 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 25-02-2021 18:02:51 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 25-02-2021 18:02:23 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 25-02-2021 18:02:23 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 25-02-2021 18:02:26 --------------------> 
123

/ajax/addBasket.php:20  - 25-02-2021 18:02:29 --------------------> 
[29] Домашние Тапочки Розовый Рай 465

/ajax/addBasket.php:23  - 25-02-2021 18:02:29 --------------------> 
Array
(
    [0] => [29]
    [1] => 29
)


/ajax/addBasket.php:27  - 25-02-2021 18:02:29 --------------------> 
123

/ajax/addBasket.php:31  - 25-02-2021 18:02:29 --------------------> 
1234

/ajax/addBasket.php:39  - 25-02-2021 18:02:29 --------------------> 
12345

/ajax/addBasket.php:20  - 25-02-2021 18:02:29 --------------------> 
[33] Пантолеты Кости на Пляже 502

/ajax/addBasket.php:23  - 25-02-2021 18:02:29 --------------------> 
Array
(
    [0] => [33]
    [1] => 33
)


/ajax/addBasket.php:27  - 25-02-2021 18:02:29 --------------------> 
123

/ajax/addBasket.php:31  - 25-02-2021 18:02:29 --------------------> 
1234

/ajax/addBasket.php:39  - 25-02-2021 18:02:29 --------------------> 
12345

/ajax/ajax_primelabs_cdek.php:18  - 25-02-2021 18:02:00 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 25-02-2021 18:02:19 --------------------> 
123

/ajax/addBasket.php:20  - 25-02-2021 18:02:21 --------------------> 
[29] Домашние Тапочки Розовый Рай 465

/ajax/addBasket.php:23  - 25-02-2021 18:02:21 --------------------> 
Array
(
    [0] => [29]
    [1] => 29
)


/ajax/addBasket.php:27  - 25-02-2021 18:02:21 --------------------> 
123

/ajax/addBasket.php:31  - 25-02-2021 18:02:21 --------------------> 
1234

/ajax/addBasket.php:39  - 25-02-2021 18:02:21 --------------------> 
12345

/ajax/ajax_primelabs_cdek.php:18  - 25-02-2021 18:02:53 --------------------> 
123

/ajax/addBasket.php:20  - 25-02-2021 18:02:54 --------------------> 
[29] Домашние Тапочки Розовый Рай 465

/ajax/addBasket.php:23  - 25-02-2021 18:02:54 --------------------> 
Array
(
    [0] => [29]
    [1] => 29
)


/ajax/addBasket.php:27  - 25-02-2021 18:02:54 --------------------> 
123

/ajax/addBasket.php:31  - 25-02-2021 18:02:55 --------------------> 
1234

/ajax/addBasket.php:39  - 25-02-2021 18:02:55 --------------------> 
12345

/ajax/ajax_primelabs_cdek.php:18  - 25-02-2021 18:02:31 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 25-02-2021 18:02:31 --------------------> 
123

/ajax/ajax_primelabs_cdek.php:18  - 25-02-2021 18:02:48 --------------------> 
123

/ajax/addBasket.php:20  - 25-02-2021 18:02:50 --------------------> 
[33] Пантолеты Кости на Пляже 502

/ajax/addBasket.php:23  - 25-02-2021 18:02:50 --------------------> 
Array
(
    [0] => [33]
    [1] => 33
)


/ajax/addBasket.php:27  - 25-02-2021 18:02:50 --------------------> 
123

/ajax/addBasket.php:31  - 25-02-2021 18:02:51 --------------------> 
1234

/ajax/addBasket.php:39  - 25-02-2021 18:02:51 --------------------> 
12345

/ajax/productSearch.php:18  - 28-02-2021 12:02:10 --------------------> 
123

/ajax/productSearch.php:18  - 28-02-2021 12:02:10 --------------------> 
123

/ajax/productSearch.php:18  - 28-02-2021 12:02:46 --------------------> 
123

/ajax/productSearch.php:18  - 28-02-2021 12:02:27 --------------------> 
123

/ajax/productSearch.php:18  - 28-02-2021 12:02:27 --------------------> 
123

/ajax/productSearch.php:18  - 28-02-2021 12:02:30 --------------------> 
123

