<?php

function debugFile($data, $fileName = 'logs.log', $logsDir = '/') {

	$message = !is_scalar($data) ? print_r($data, 1) : $data;
	$log_path = $_SERVER['DOCUMENT_ROOT']. $logsDir . $fileName;

	if (!file_exists($log_path)) {
		touch($log_path);
	}

	$info = debug_backtrace();
	$info = $info[0];
	$info['file'] = substr($info['file'], strlen($_SERVER['DOCUMENT_ROOT']));
	$where = "{$info['file']}:{$info['line']}  - ". date('d-m-Y H:m:s');
	$str = $where .' '. str_pad('', 20, '-') . "> \r\n" . $message . "\r\n\r\n";

	file_put_contents($log_path, $str, FILE_APPEND);
}
