/** @param {HTMLElement} element */
function ProductSearch(element, sessid) {

    this.element = element;
    this.queryElement = element.querySelector('.search-query');
    this.resultElement = element.querySelector('.search-result');
    this.sessid = sessid;
    this.xhr = new XMLHttpRequest();

}

ProductSearch.prototype.addEvents = function() {
    const self = this;
    let timerId = 0;
    this.queryElement.addEventListener('click', ev => {
        if (self.queryElement.value) {
            return;
        }
        if (timerId) {
            clearTimeout(timerId);
            self.xhr.abort();
        }
        timerId = setTimeout(() => {
            self.query(self.queryElement.value);
            clearTimeout(timerId);
        }, 1000);
    });
    this.queryElement.addEventListener('keyup', ev => {
        if (timerId) {
            clearTimeout(timerId);
            self.xhr.abort();
        }
        timerId = setTimeout(() => {
            self.query(self.queryElement.value);
            clearTimeout(timerId);
        }, 1000);
    });
    document.addEventListener('click', (ev) => {
        let el = ev.target;
        while (el && el !== document) {
            if (el === self.resultElement) {
                return;
            }
            el = el.parentElement;
        }
        self.resultElement.classList.remove('visible');
    });
    this.resultElement.addEventListener('click', (ev) => {
        let value = '';
        let el = ev.target;
        while (el && el !== self.resultElement) {
            if (el.tagName == 'LI') {
                value = el.dataset.value;
                break;
            }
            el = el.parentElement;
        }
        self.resultElement.classList.remove('visible');
        self.queryElement.value = value;
    });
}

ProductSearch.prototype.query = function (query) {
    const self = this;
    this.resultElement.classList.remove('visible');
    if (self.element.classList.contains('XML_ID')) {
        this.request([query], response => {
            if (response.error) {
                console.log(response.error);
                return;
            }
            console.log(response);
            let html = '<ul>';
            for (let item of response.result) {
                html += `<li data-value="[${item.ID}] ${item.NAME} ${item.XML_ID}">[${item.ID}] <span style="color: gray">` + item.NAME + `</span> ${item.XML_ID}</li>`;
            }
            html += '</ul>';
            self.resultElement.innerHTML = html;
            self.resultElement.classList.add('visible');
        });
    }
}

ProductSearch.prototype.request = function (parameters, callback) {
    const self = this;
    console.log(this.sessid);
    console.log(parameters);
    this.xhr.open('POST', '/ajax/productSearch.php');
    this.xhr.onload = ev => {
        if (self.xhr.status >= 400) {
            return callback({error: {message: `<p style="color: red;">Произошла ошибка: ${self.xhr.status} ${self.xhr.statusText}</p>`}});
        }
        if (self.xhr.responseText) {
            return callback(JSON.parse(self.xhr.responseText));
        }
        return false;
    };
    this.xhr.setRequestHeader('Content-Type', 'application/json');
    this.xhr.send(JSON.stringify({sessid: this.sessid, parameters}));
}
