<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST' && $_SERVER['CONTENT_TYPE'] == 'application/json') {
    if ($json = file_get_contents('php://input')) {
        $_POST = array_merge($_POST, json_decode($json, true));
        $_REQUEST = array_merge($_REQUEST, json_decode($json, true));
    } else {
        throw new Exception('empty post request body');
    }
}

require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php';

use Bitrix\Main\Loader;

require('/opt/lampp/htdocs/debugFile.php');

debugFile(123);

/*use PrimeLabs\Cdek\Type\Element;

$MODULE_ID = 'primelabs.cdek';
$MODULE_RIGHT = $APPLICATION->GetGroupRight($MODULE_ID);
if ($MODULE_RIGHT == 'D') {
    http_response_code(403);
    exit;
}
Loader::includeModule('primelabs.cdek');

if ($_POST['method'])
{
    list($object, $method) = explode('.', $_POST['method']);
    $parameters =  $_POST['parameters'] ?: [];
    try {

        if ($object == 'element' && method_exists(Element::class, $method)) {
            $result = call_user_func_array([Element::class, $method], $parameters);
        } elseif ($object == 'section' && method_exists(Section::class, $method)) {
            $result = call_user_func_array([Section::class, $method], $parameters);
        } else {
            throw new Exception('unknown', 404);
        }
        http_response_code(200);
        header('Content-Type: application/json');
        echo json_encode(['result' => $result]);
    } catch (Exception $exception) {
        http_response_code($exception->getCode());
        header('Content-Type: application/json');
        echo json_encode(['error' => ['code' => $exception->getCode(), 'message' => $exception->getMessage()]]);
    }
}*/

require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_after.php';
