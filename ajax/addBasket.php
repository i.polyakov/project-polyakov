<?
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
require('/opt/lampp/htdocs/debugFile.php');

use Bitrix\Main\Loader;

Loader::includeModule('sale');
Loader::includeModule('catalog');

$PRODUCT_ID = $_POST['inputtext'];

$output = [];

foreach ($PRODUCT_ID as $value) {

    preg_match('/\[(\w+)\]/', $value, $output);

    $product = array('PRODUCT_ID' => $output[1], 'QUANTITY' => 1);

    $result = Bitrix\Catalog\Product\Basket::addProduct($product);

    if (!$result->isSuccess()) { //ну и проверочка, для дебага
        debugFile($result->getErrorMessages());
    }
}
?>