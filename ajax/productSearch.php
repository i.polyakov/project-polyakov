<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST' && $_SERVER['CONTENT_TYPE'] == 'application/json') {
    if ($json = file_get_contents('php://input')) {
        $_POST = array_merge($_POST, json_decode($json, true));
        $_REQUEST = array_merge($_REQUEST, json_decode($json, true));
    } else {
        throw new Exception('empty post request body');
    }
}

require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php';

use Bitrix\Main\Loader;
Loader::includeModule('iblock');

require('/opt/lampp/htdocs/debugFile.php');

debugFile(123);

$arFields = [];

$arSelect = Array("ID", "NAME", "XML_ID");
$arFilter = Array("IBLOCK_ID"=> 2, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "XML_ID" => $_POST['parameters']);
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>5), $arSelect);
while($ob = $res->GetNextElement())
{

    $arFields[] = $ob->GetFields();
}

http_response_code(200);
header('Content-Type: application/json');
echo json_encode(['result' => array_values($arFields)]);

require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_after.php';
