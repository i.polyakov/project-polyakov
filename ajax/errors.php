<?
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
CModule::IncludeModule("iblock");

$PROP['TEXT_ERRORS'] = $_POST['text_errors'];
$PROP['PAGE'] = $_POST['page'];
$PROP['STATUS'] = 6;

$el = new CIBlockElement;

$arLoadProductArray = Array(
  "MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
  "IBLOCK_SECTION_ID" => false,          // элемент лежит в корне раздела
  "IBLOCK_ID"      => 3,
  "PROPERTY_VALUES"=> $PROP,
  "NAME"           => "Элемент",
  "ACTIVE"         => "Y",            // активен
  "PREVIEW_TEXT"   => "текст для списка элементов",
  "DETAIL_TEXT"    => "текст для детального просмотра",
  );

$el->Add($arLoadProductArray);

?>